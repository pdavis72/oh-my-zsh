_rake_refresh () {
  if [ -f .rake_tasks ]; then
    rm .rake_tasks
  fi
  echo "Generating .rake_tasks..." > /dev/stderr
  _rake_generate
  cat .rake_tasks
}

_rake_does_task_list_need_generating () {
<<<<<<< HEAD
  [[ ! -f .rake_tasks ]] || [[ Rakefile -nt .rake_tasks ]]
=======
<<<<<<< HEAD
  [[ ! -f .rake_tasks ]] || [[ Rakefile -nt .rake_tasks ]]
=======
  if [ ! -f .rake_tasks ]; then return 0;
  else
    if [[ "$OSTYPE" = darwin* ]]; then
      accurate=$(stat -f%m .rake_tasks)
      changed=$(stat -f%m Rakefile)
    else
      accurate=$(stat -c%Y .rake_tasks)
      changed=$(stat -c%Y Rakefile)
    fi
    return $(expr $accurate '>=' $changed)
  fi
>>>>>>> c0134a9450e486251b247735e022d7efeb496b9c
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
}

_rake_generate () {
  rake --silent --tasks | cut -d " " -f 2 > .rake_tasks
}

_rake () {
  if [ -f Rakefile ]; then
    if _rake_does_task_list_need_generating; then
      echo "\nGenerating .rake_tasks..." > /dev/stderr
      _rake_generate
    fi
    compadd `cat .rake_tasks`
  fi
}

compdef _rake rake
alias rake_refresh='_rake_refresh'
