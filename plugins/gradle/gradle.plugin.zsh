##############################################################################
<<<<<<< HEAD
# A descriptive listing of core Gradle commands
=======
<<<<<<< HEAD
# A descriptive listing of core Gradle commands
=======
# A descriptive listing of core Gradle commands 
>>>>>>> c0134a9450e486251b247735e022d7efeb496b9c
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
############################################################################
function _gradle_core_commands() {
    local ret=1 state
    _arguments ':subcommand:->subcommand' && ret=0

    case $state in
      subcommand)
        subcommands=(
          "properties:Display all project properties"
          "tasks:Calculate and display all tasks"
          "dependencies:Calculate and display all dependencies"
          "projects:Discover and display all sub-projects"
          "build:Build the project"
          "help:Display help"
        )
        _describe -t subcommands 'gradle subcommands' subcommands && ret=0
    esac

    return ret
}

function _gradle_arguments() {
    _arguments -C \
    '-a[Do not rebuild project dependencies]' \
    '-h[Help]' \
    '-D[System property]' \
    '-d[Log at the debug level]' \
    '--gui[Launches the Gradle GUI app]' \
    '--stop[Stop the Gradle daemon]' \
    '--daemon[Use the Gradle daemon]' \
    '--no-daemon[Do not use the Gradle daemon]' \
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
    '--rerun-task [Specifies that any task optimization is ignored.]' \
    '-i[Log at the info level]' \
    '-m[Dry run]' \
    '-P[Set a project property]' \
    '-p[Specifies the start directory]' \
<<<<<<< HEAD
=======
=======
    '--no-opt[Do not perform any task optimization]' \
    '-i[Log at the info level]' \
    '-m[Dry run]' \
    '-P[Set a project property]' \
>>>>>>> c0134a9450e486251b247735e022d7efeb496b9c
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
    '--profile[Profile the build time]' \
    '-q[Log at the quiet level (only show errors)]' \
    '-v[Print the Gradle version info]' \
    '-x[Specify a task to be excluded]' \
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
    '-b[Specifies the build file.]' \
    '-c[Specifies the settings file.]' \
    '--continue[Continues task execution after a task failure.]' \
    '-g[Specifies the Gradle user home directory.]' \
    '-I[Specifies an initialization script.]' \
    '--refresh-dependencies[Refresh the state of dependencies.]' \
    '-u[Don''t search in parent directories for a settings.gradle file.]' \
<<<<<<< HEAD
=======
=======
>>>>>>> c0134a9450e486251b247735e022d7efeb496b9c
>>>>>>> f4626a93a73ef09e2a5fc5801581c557459682fb
    '*::command:->command' \
    && return 0
}


##############################################################################
# Examine the build.gradle file to see if its timestamp has changed;
# and if so, regenerate the .gradle_tasks cache file
############################################################################
_gradle_does_task_list_need_generating () {
  [[ ! -f .gradletasknamecache ]] || [[ build.gradle -nt .gradletasknamecache ]]
}


##############################################################################
# Discover the gradle tasks by running "gradle tasks --all"
############################################################################
_gradle_tasks () {
  if [[ -f build.gradle ]]; then
    _gradle_arguments
    if _gradle_does_task_list_need_generating; then
      gradle tasks --all | awk '/[a-zA-Z0-9:-]* - / {print $1}' > .gradletasknamecache
    fi
    compadd -X "==== Gradle Tasks ====" $(cat .gradletasknamecache)
  fi
}

_gradlew_tasks () {
  if [[ -f build.gradle ]]; then
    _gradle_arguments
    if _gradle_does_task_list_need_generating; then
      ./gradlew tasks --all | awk '/[a-zA-Z0-9:-]* - / {print $1}' > .gradletasknamecache
    fi
    compadd -X "==== Gradlew Tasks ====" $(cat .gradletasknamecache)
  fi
}


##############################################################################
# Register the completions against the gradle and gradlew commands
############################################################################
compdef _gradle_tasks gradle
compdef _gradlew_tasks gradlew
